package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.entity.ReportForm;

public interface ReportFormRepository extends JpaRepository<ReportForm,Integer>{

	@Query(value = "SELECT * FROM REPORTFORM WHERE USERID = ? AND REPSTATUS = 0", nativeQuery = true)
	List<ReportForm> findAlluserid(String userName);
	
	@Query(value = "SELECT * FROM REPORTFORM WHERE SANMOJI = ?", nativeQuery = true)
	List<ReportForm> findAllsanmoji(String sanmoji);

	@Query(value = "SELECT * FROM REPORTFORM WHERE REPSTATUS = 0", nativeQuery = true)
	List<ReportForm> findAllstatus();
}

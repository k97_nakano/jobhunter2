package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, String> {
	@Query("FROM Account a WHERE a.userid = :userid")
	Account findByUsername(@Param("userid") String userid);
	
	int countByUserid(String userid);
	
	String userName = null;
	@Query(value = "SELECT STD_EM FROM ACCOUNTS WHERE USERID = ?", nativeQuery = true)
	String judgmentstudent(String userName);
	
	
	@Query(value = "SELECT TEACHER FROM ACCOUNTS WHERE USERID = ?", nativeQuery = true)
	String judgmentteacher(String userName);
	
	@Query(value = "SELECT NAME FROM ACCOUNTS WHERE USERID = ?", nativeQuery = true)
	String findName(String Name);
}

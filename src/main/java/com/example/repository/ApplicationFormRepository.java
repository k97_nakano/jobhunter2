package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.entity.ApplicationForm;

@Repository
public interface ApplicationFormRepository extends JpaRepository<ApplicationForm,Integer>{
	
	@Query(value = "SELECT * FROM APPLICATIONFORM WHERE USERID = ? AND APPSTATUS = 0", nativeQuery = true)
	List<ApplicationForm> findAlluserid(String userName);
	
	@Query(value = "SELECT * FROM APPLICATIONFORM WHERE APPSTATUS = 0", nativeQuery = true)
	List<ApplicationForm> findAllstatus();

}
